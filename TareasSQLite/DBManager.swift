//
//  DBManager.swift
//  PruebaSQLite
//
//  Created by Otto Colomina Pardo on 18/12/16.
//  Copyright © 2016 Universidad de Alicante. All rights reserved.
//

import Foundation


class DBManager {
    var db : OpaquePointer? = nil
    
    init(conDB nombreDB : String, reload: Bool) {
        if let dbCopiaURL = copyDB(conNombre: nombreDB, reload: reload) {
            if sqlite3_open(dbCopiaURL.path, &(self.db)) == SQLITE_OK {
                print("Base de datos \(dbCopiaURL) abierta OK")
            }
            else {
                let error = String(cString:sqlite3_errmsg(db))
                print("Error al intentar abrir la BD: \(error) ")
            }
        }
        else {
            print("El archivo no se encuentra")
        }
    }
    
    deinit {
        sqlite3_close(self.db)
    }
    
    func copyDB(conNombre nombre : String, reload machaca : Bool)->URL? {
        let fileManager = FileManager.default
        let docsURL = fileManager.urls(for: .documentDirectory, in: .userDomainMask)[0]
        let dbCopiaURL = docsURL.appendingPathComponent(nombre).appendingPathExtension("db")
        let existe = fileManager.fileExists(atPath: dbCopiaURL.path)
        if  existe && !machaca {
            return dbCopiaURL
        }
 
        else {
            if let dbOriginalURL = Bundle.main.url(forResource: nombre, withExtension: "db") {
                if (existe) {
                  try! fileManager.removeItem(at: dbCopiaURL)
                }
                if (try? fileManager.copyItem(at: dbOriginalURL, to: dbCopiaURL)) != nil {
                    return dbCopiaURL
                }
                else {
                    return nil
                }
            }
            else {
                return nil
            }
        }
    }
    
    
}
