//
//  Tarea.swift
//  TareasSQLite
//
//  Created by Master Móviles on 15/2/17.
//  Copyright © 2017 Master Móviles. All rights reserved.
//

import Foundation

struct Tarea {
    var id: Int
    var titulo: String
    var prioridad: Int
    var vencimiento: Date
}
