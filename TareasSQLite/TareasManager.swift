//
//  TareasManager.swift
//  TareasSQLite
//
//  Created by Master Móviles on 15/2/17.
//  Copyright © 2017 Master Móviles. All rights reserved.
//

import Foundation

class TareasManager: DBManager {
    func listarTareas() -> [Tarea] {
        let querySQL = "SELECT * FROM Tareas ORDER BY vencimiento"
        var statement: OpaquePointer?
        var lista: [Tarea] = []
        
        let result = sqlite3_prepare_v2(self.db, querySQL, -1, &statement, nil)
        if result == SQLITE_OK {
            while sqlite3_step(statement) == SQLITE_ROW {
                let id = sqlite3_column_int(statement, 0)
                let titulo = String(cString: sqlite3_column_text(statement, 1))
                let prioridad = sqlite3_column_int(statement, 2)
                let unix_time = sqlite3_column_int(statement, 3)
                
                let fecha = Date(timeIntervalSince1970: TimeInterval(unix_time))
                
                let persona = Tarea(id: Int(id), titulo: titulo, prioridad: Int(prioridad), vencimiento: fecha)
                lista.append(persona)
            }
        }
        sqlite3_close(statement)
        
        return lista
    }
    
    func insertar(tarea: Tarea) -> Bool {
        let querySQL = "INSERT INTO Tareas (titulo, prioridad, vencimiento) VALUES (?,?,?)"
        var statement : OpaquePointer?
        
        sqlite3_prepare_v2(db, querySQL, -1, &statement, nil);
        
        sqlite3_bind_text(statement, 1, tarea.titulo, -1, nil);
        sqlite3_bind_int(statement, 2, Int32(tarea.prioridad));
        sqlite3_bind_int(statement, 3, Int32(tarea.vencimiento.timeIntervalSince1970));

        let result = sqlite3_step(statement);
        
        return result==SQLITE_DONE
    }
}
